/*******************************************************************
  Hold and edit current state of application.


  reader_state.c

  Author:       Ondřej Tůma 
  Copyright (C) 2020  Ondřej Tůma

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

 *******************************************************************/

#include "reader_state.h"

state *init_state() {
  state *new_state = (state *)malloc(sizeof(state));
  new_state->state = 'h';
  new_state->articles_storage = (article **)malloc(sizeof(article *) * 10);
  new_state->selected_article = 0;
  uint32_t x;
  new_state->usage_file = load_file("usage.txt", &x);
  new_state->articles_count = 0;
  new_state->font_scale = 1;
  new_state->unknown_command = true;
  new_state->led_strip = 0;
  return new_state;
}

void reset_state(state *app_state) {
  app_state->state = 'h';
  app_state->led_strip = 0;
  app_state->current_reading_start = NULL;
  app_state->current_reading_end = NULL;
}

void state_to_string(state *st, char *output) {
  char line[33];
  bin32_to_s(line, st->led_strip);
  sprintf(output,
          "state:             %c\n"
          "articles count:    %d\n"
          "selected article:  %d\n"
          "article headline:  %s\n"
          "font scale:        %d\n"
          "led strip:         %s\n"
          "reading start:     %p\n"
          "reading end:       %p\n",
          st->state,
          st->articles_count, st->selected_article,
          st->articles_storage[st->selected_article]->head, st->font_scale,
          line, st->current_reading_start, st->current_reading_end);
}