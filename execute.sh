
ssh tumaond2@postel.felk.cvut.cz "rm -rf semestralka; mkdir -p semestralka/articles; mkdir -p semestralka/fallback_articles"
scp fallback_articles/* tumaond2@postel.felk.cvut.cz:semestralka/fallback_articles/
scp Makefile *.h *.c usage.txt *.py tumaond2@postel.felk.cvut.cz:semestralka
ssh tumaond2@postel.felk.cvut.cz -a -t "cd semestralka && make run"
