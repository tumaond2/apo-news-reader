/*******************************************************************
  General tools for generic repetitive actions.


  general_tools.h

  Author:       Ondřej Tůma 
  Copyright (C) 2020  Ondřej Tůma

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

 *******************************************************************/

#ifndef GENERAL_TOOLS
#define GENERAL_TOOLS

#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>


#define START_RED "\033[1;31m"
#define START_GREEN "\033[1;32m"
#define END_COLOR "\033[0m"
#define DIVIDER_SMALL "\033[1;7m---------------------------------------------------------------------\033[0m"
#define DIVIDER                                                                \
  "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n"\
  "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n"

/**
 * @brief load file as char array
 * 
 * @return char* 
 */
char *load_file(char *, uint32_t *);

/**
 * @brief print binary to stdout
 * 
 * @param int 
 */
void print_bin(void *, long int);

/**
 * @brief reset terminal
 * 
 */
void clear_terminal();

/**
 * @brief print text in red
 * 
 */
void print_red(char *);

/**
 * @brief print text in green
 * 
 */
void print_green(char *);

/**
 * @brief uint 32 to string
 * 
 */
void bin32_to_s(char *, uint32_t);

#endif