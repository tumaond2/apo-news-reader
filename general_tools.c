/*******************************************************************
  General tools for generic repetitive actions.


  general_tools.c

  Author:       Ondřej Tůma 
  Copyright (C) 2020  Ondřej Tůma

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

 *******************************************************************/

#include "general_tools.h"

char *load_file(char *filename, uint32_t *size) {
  printf("%s FILE LOADER\nloading file: %s\n",DIVIDER_SMALL, filename);
  int usage = open(filename, O_RDONLY);
  assert(usage);
  struct stat sb;
  stat(filename, &sb);
  *size = sb.st_size;
  char *data_orig = (char *)calloc(*size + 1, 1);
  printf("read: %d\n", read(usage, data_orig, *size));
  close(usage);
  return data_orig;
}

void print_bin(void *ap, long int size) {
  unsigned long long var_a = *((unsigned long long *)(ap));
  unsigned int i = 0;
  unsigned long long mask = 1;
  mask = mask << (size * 8 - 1);
  for (i = 0; i<size * 8; i++, mask = mask>> 1) {
    var_a &mask ? putc('1', stdout) : putc('0', stdout);
  }
}


void bin32_to_s(char *buffer, uint32_t number) {
  int size = 4;
  buffer[size * 8] = 0;
  unsigned int i = 0;
  unsigned long long mask = 1;
  mask = mask << (size * 8 - 1);
  for (i = 0; i<size * 8; i++, mask = mask>> 1) {
    if (number &mask ) {
      buffer[i] = '1';
    } else {
      buffer[i] = '0';
    }
  }

}

void clear_terminal() {
  const char *CLEAR_SCREEN_ANSI = "\e[1;1H\e[2J";
  puts(DIVIDER);
  write(STDOUT_FILENO, CLEAR_SCREEN_ANSI, 12);
}

void print_red(char *string) {
  printf("%s", START_RED);
  printf("%s", string);
  printf("%s", END_COLOR);
}
void print_green(char *string) {
  printf("%s", START_GREEN);
  printf("%s", string);
  printf("%s", END_COLOR);
}

