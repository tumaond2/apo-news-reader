import requests
import bs4 as bs
import xml.etree.ElementTree as ET
import re

class Scrapper:
    charmap = {
            'á' : 'a',
            'é' : 'e',
            'í' : 'i',
            'ó' : 'o',
            'ú' : 'u',
            'ů' : 'u',
            'ý' : 'y',

            'ž' : 'z',
            'š' : 's',
            'č' : 'c',
            'ř' : 'r',
            'ď' : 'd',
            'ť' : 't',
            'ň' : 'n',
            'ě' : 'e',

            'Á' : 'A',
            'É' : 'E',
            'Í' : 'I',
            'Ó' : 'O',
            'Ú' : 'U',
            'Ů' : 'U',
            'Ý' : 'Y',

            'Ž' : 'Z',
            'Š' : 'S',
            'Č' : 'C',
            'Ř' : 'R',
            'Ď' : 'D',
            'Ť' : 'T',
            'Ň' : 'N',
            'Ě' : 'E',


            '\u0119' : 'e',
            '\xa0' : ' ',
            '\u017a' : 'z',
            '\u201e' : '\x22',
            '\u201c' : '\x22',
            '\u2026' : "...",
            '–': '-',
            '”': '\x22',
            'ü': 'u',
            'ö' : 'o',
            'ć' : 'c',
            'ä' : 'a',
            ' ' : ' ',
            '­' : '',

        }

    def get_last_articles(self, number):
        URL = 'https://www.novinky.cz/rss'
        page = requests.get(URL)
        doc = bs.BeautifulSoup(page.text, 'xml')
        items = doc.findAll("item")
        return [item for item in items[:number]]

    def get_text_for_article(self, article_xml):
        link = article_xml.find("link").contents[0];
        article = requests.get(link)
        article = bs.BeautifulSoup(article.text, 'html.parser')
        paragraphs = article.find_all('p', class_="d_c-")
        text = ""
        for paragraph in paragraphs:
            if 'd_aa' in paragraph.get('class') or 'd_H' in paragraph.get('class'):
                continue
            else:
                text += self.strip_czech_chars(paragraph.get_text()) + "\n"
        return text

    def gen_title_text_pairs(self, articles):
        output = []
        for article in articles:
            title = article.find("title").contents[0]
            text = self.get_text_for_article(article)
            output.append((title, text))
        return output
    def strip_czech_chars(self, string):
        output = ""
        string = re.sub(r'[\\/*?"<>|]',"",string)
        for x in string:
            if x in self.charmap:
                output += self.charmap[x];
            elif ord(x) < 128:
                output += x
            else: 
                print("skipping unsuported character: " + x + " : " + str(ord(x)))
        return output;
if __name__ == "__main__":
    sc = Scrapper()
    articles = sc.get_last_articles(10)
    output = sc.gen_title_text_pairs(articles)
    for article in output:
        f = open(("articles/" + sc.strip_czech_chars(article[0])).encode('ascii'), "wb")
        f.write(article[1].encode('ascii'))
