/*******************************************************************
  A set of tools and parsers for parsing commands from commandline

  commands.h

  Author:       Ondřej Tůma 
  Copyright (C) 2020  Ondřej Tůma

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

 *******************************************************************/

#ifndef COMMANDS
#define COMMANDS


#include "general_tools.h"
#include "reader_state.h"


#define COMMAND_DELIMETER '='
#define COMMAND_SCALE "scale"
#define COMMAND_COLOR "color"
#define COMMAND_READ "read"
#define COMMAND_BACK "back"
#define COMMAND_NEXT "next"
#define COMMAND_PREV "prev"
#define COMMAND_LOGO "logo"
#define COMMAND_SCROLL "scroll"
#define COMMAND_HELP "help"
#define COMMAND_QUIT "quit"

/**
 * @brief await for user input, parse command and change state accordingly
 * 
 */
void await_command(state *);

#endif