---------------------------------------------------------------
BASIC USAGE:
    command=params

COMMAND LIST:
    GENERAL:
        -------------------------------------------------------
        help
            params:         none
            usage:          help
            description:    displa this help page
        -------------------------------------------------------
        scale
            params:         single positive digit
            usage:          scale=5
            description:    set text scaling
        -------------------------------------------------------
        font_color
            params:         16 digit binary number
            usage:          color=1111111111111111
            description:    set color for all renders
        -------------------------------------------------------
        back
            params:         none
            usage:          back
            description:    back to headline mode
        -------------------------------------------------------
        logo
            params:         none
            usage:          logo
            description:    enter logo mode
        -------------------------------------------------------
        quit
            params:         none
            usage:          quit
            description:    quit application
        -------------------------------------------------------
    -----------------------------------------------------------        
    HEADLINE MODE ONLY:
        -------------------------------------------------------
        read
            params:         none
            usage:          read
            description:    enter read mode for current article
        -------------------------------------------------------
        scroll
            params:         [down, up]
            usage:          scroll=down
            description:    Scroll down to next page
                            Scroll to the start of article
        -------------------------------------------------------
        next
            params:         none
            usage:          next
            description:    Go to next headline
        -------------------------------------------------------
        prev
            params:         none
            usage:          next
            description:    Go to previous headline
        -------------------------------------------------------
    -----------------------------------------------------------
    
---------------------------------------------------------------