/*******************************************************************
  Controller for MZAPO board periphrals.


  display_controller.c

  Author:       Ondřej Tůma 
  Copyright (C) 2020  Ondřej Tůma

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

 *******************************************************************/

#define _POSIX_C_SOURCE 200112L

#include "display_controller.h"

#include "font_rom8x16.c"
#include "font_types.h"
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

unsigned char *mem_base;
unsigned char *parlcd_mem_base;
uint16_t *display_buffer;
font_descriptor_t selected_font;
int font_scale;
uint16_t selected_display_color;

void set_display_color(uint16_t color) { selected_display_color = color; }

void set_font_scale(int scale) { font_scale = scale; }

int sign(int x) {
  if (x < 0)
    return -1;
  else
    return 1;
}

uint16_t rgb_to_pixel(unsigned char r, unsigned char g, unsigned char b) {
  if (r > 127) r = 127;
  if (g > 127) g = 127;
  if (b > 127) b = 127;
  return (r << 11) + (g << 5) + b;
}

void print_buffer() {
  parlcd_write_cmd(parlcd_mem_base, 0x2c);
  for (int i = 0; i < SCREEN_HEIGHT; i++) {
    for (int j = 0; j < SCREEN_WIDTH; j++) {
      parlcd_write_data(parlcd_mem_base, display_buffer[i * SCREEN_WIDTH + j]);
    }
  }
}

void fill_buffer(uint16_t color) {
  for (int i = 0; i < SCREEN_SIZE; i++) {
    display_buffer[i] = color;
  }
}

void reset_buffer() { fill_buffer(DISPLAY_COLOR_BLACK); }

void add_line(int x0, int y0, int x1, int y1) {
  int dx = abs(x1 - x0);
  int sx = x0 < x1 ? 1 : -1;
  int dy = -abs(y1 - y0);
  int sy = y0 < y1 ? 1 : -1;
  int err = dx + dy;
  while (1) {
    display_buffer[y0 * SCREEN_WIDTH + x0] = selected_display_color;
    if (x0 == x1 && y0 == y1) break;
    int e2 = 2 * err;
    if (e2 >= dy) {
      err += dy;
      x0 += sx;
    }
    if (e2 <= dx) {
      err += dx;
      y0 += sy;
    }
  }
}

void add_rectangle(int x0, int y0, int x1, int y1) {
  add_line(x0, y0, x1, y0);  // top
  add_line(x0, y1, x1, y1);  // bot
  add_line(x0, y0, x0, y1);  // left
  add_line(x1, y0, x1, y1);  // right
}

void add_letter(int x, int y, char c) {
  const uint16_t *start = &selected_font.bits[c * selected_font.height];
  for (uint16_t row = 0; row < selected_font.height * font_scale; row++) {
    for (uint16_t col = 0; col < selected_font.maxwidth * font_scale; col++) {
      display_buffer[(y + row) * SCREEN_WIDTH + x + col] =
          ((start[(int)(row / font_scale)] >>
            (16 - ((int)(col / font_scale)))) &
           1) *
          selected_display_color;
    }
  }
}

void add_string(int x, int y, char *string) {
  for (int i = 0; string[i]; i++) {
    add_letter(x + (i * selected_font.maxwidth * font_scale), y, string[i]);
  }
}

/**
 * @brief length of word
 *
 * @param text
 * @return int
 */
int word_length(char *text) {
  int i = 0;
  for (; text[i] != ' ' && text[i] && text[i] != '\n'; i++) {
  }
  return i;
}
/**
 * @brief word can be printed on the current line
 *
 * @param text
 * @param x
 * @return true
 * @return false
 */
bool word_fits(char *text, int x) {
  return (bool)(x + word_length(text) * (selected_font.maxwidth * font_scale) <
                SCREEN_WIDTH);
}

/**
 * @brief Print single word to screen
 *
 * @param text
 * @param x
 * @param y
 * @return int characters printed
 */
int print_word(char *text, int x, int y) {
  int i = 0;
  int location = x;
  // printf("to printf: %c\n", text[i]);
  for (; text[i] && text[i] != ' ' && text[i] != '\n' &&
         location < SCREEN_WIDTH - (selected_font.maxwidth * font_scale);
       i++) {
    add_letter(location, y, text[i]);
    location += (selected_font.maxwidth * font_scale);
  }
  return i;
}

/**
 * @brief Fill line with text while it fits
 *
 * @param text
 * @param x
 * @param y
 * @return char* first character that did not fit on line
 */
char *fill_line_with_text(char *text, int x, int y) {
  // printf("fill line with text\n");
  int location = x;
  // printf("word fits: %d\n", word_fits(text, location));
  // printf("equ: %d\n", word_length(text) * (selected_font.maxwidth *
  // font_scale) > SCREEN_WIDTH);
  for (; (word_fits(text, location) ||
          word_length(text) * (selected_font.maxwidth * font_scale) >=
              SCREEN_WIDTH) &&
         *text;) {
    // printf("iteration\n");
    int len = word_length(text);
    int printed_chars = print_word(text, location, y);
    text += printed_chars;
    location += printed_chars * (selected_font.maxwidth * font_scale);
    if (len != printed_chars) break;
    if (*text == ' ') {
      add_letter(location, y, *text);
      text++;
      location += (selected_font.maxwidth * font_scale);
    } else if (*text == '\n') {
      text++;
      break;
    }
  }
  return text;
}

char *fill_display_with_text(char *text) {
  int y = 0;
  int line_height = selected_font.height * font_scale;
  for (int i = 0; *text && y < SCREEN_HEIGHT - line_height; i++) {
    text = fill_line_with_text(text, 0, y);
    y += line_height;
  }
  return text;
}

void set_strip(uint32_t value) {
  *(volatile uint32_t *)(mem_base + SPILED_REG_LED_LINE_o) = value;
}

void set_left_led(uint32_t color) {
  *(volatile uint32_t *)(mem_base + SPILED_REG_LED_RGB1_o) = color;
}

void set_right_led(uint32_t color) {
  *(volatile uint32_t *)(mem_base + SPILED_REG_LED_RGB2_o) = color;
}

void init_display() {
  mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
  if (mem_base == NULL) exit(1);
  parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
  if (parlcd_mem_base == NULL) exit(1);

  parlcd_hx8357_init(parlcd_mem_base);
  display_buffer = (uint16_t *)malloc(BUFFER_SIZE);
  set_font(font_rom8x16);
  set_display_color(DISPLAY_COLOR_WHITE);
  set_font_scale(1);
}

void set_font(font_descriptor_t font) { selected_font = font; }

void render_fit(char *text) {
  for (int i = 9; i > 0; i--) {
    reset_buffer();
    char *textp = text;
    set_font_scale(i);
    textp = fill_display_with_text(textp);
    if (!*textp) {
      printf(
          "text:     %s\n"
          "scale:        %d\n",
          text, i);
      break;
    }
  }
  print_buffer();
}