/*******************************************************************
  Hold and edit current state of application.


  reader_state.h

  Author:       Ondřej Tůma 
  Copyright (C) 2020  Ondřej Tůma

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

 *******************************************************************/
#ifndef READER_STATE
#define READER_STATE

#include "general_tools.h"

typedef struct {
  char *head;
  char *body;
  uint32_t size;
} article;

typedef struct {
  char state;
  int selected_article;
  char *usage_file;
  article ** articles_storage;
  int articles_count;
  int font_scale;
  char *current_reading_start;
  char *current_reading_end;
  uint32_t led_strip;
  bool unknown_command;
} state;

/**
 * @brief set app state to predefined value
 * 
 * @return state* 
 */
state *init_state();

/**
 * @brief state to string
 * 
 */
void state_to_string(state *, char *);

/**
 * @brief reset state to headline mode
 * 
 */
void reset_state(state *);

#endif