/*******************************************************************
  A set of tools and parsers for parsing commands from commandline

  commands.c

  Author:       Ondřej Tůma 
  Copyright (C) 2020  Ondřej Tůma

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

 *******************************************************************/

#include "commands.h"
#include "display_controller.h"

void com_quit(state *app_state, char *command, char *delimeter, bool *match,
               char *error, char * info, char * command_message) {
  if (strncmp(COMMAND_QUIT, command, (delimeter - command)) == 0) {
      printf("QUITTING NOW");
      exit(0);
  }
}

void com_scale(state *app_state, char *command, char *delimeter, bool *match,
               char *error, char * info, char * command_message) {
  if (strncmp(COMMAND_SCALE, command, (delimeter - command)) == 0) {
    *match = true;

    if (!delimeter) {
      sprintf(error, "WRONG COMMAND FORMAT");
      return;
    }

    app_state->font_scale = *(delimeter + 1) - 48;
    set_font_scale(app_state->font_scale);
    sprintf(command_message, "new scale: %d\n", *(delimeter + 1) - 48);
  }
}
void com_color(state *app_state, char *command, char *delimeter, bool *match,
               char *error, char * info, char * command_message) {
  if (strncmp(COMMAND_COLOR, command, (delimeter - command)) == 0) {
    *match = true;

    if (!delimeter) {
      sprintf(error, "WRONG COMMAND FORMAT");
      return;
    }

    uint16_t code = 0;
    char buffer[17]; buffer[16] = 0;
    
    
    for (int i = 0; i < 16 && delimeter[1 + i]; i++) {
      code += ((delimeter[1 + i] - 48) & 1) << (15 - i);
      buffer[i] = (((delimeter[1 + i] - 48) & 1) + 48);
    }
    sprintf(command_message, "new color: %s\n", buffer);

    set_display_color(code);
  }
}

void com_read(state *app_state, char *command, char *delimeter, bool *match,
              char *error, char * info, char * command_message) {
  if (strncmp(COMMAND_READ, command, (delimeter - command)) == 0) {
    *match = true;
    reset_state(app_state);
    app_state->state = 'r';
    sprintf(command_message, "Entering read mode\n");
  }
}

void com_back(state *app_state, char *command, char *delimeter, bool *match,
              char *error, char * info, char * command_message) {
  if (strncmp(COMMAND_BACK, command, (delimeter - command)) == 0) {
    *match = true;
    reset_state(app_state);
    sprintf(command_message, "Entering headline mode\n");
  }
}
void com_logo(state *app_state, char *command, char *delimeter, bool *match,
              char *error, char * info, char * command_message) {
  if (strncmp(COMMAND_LOGO, command, (delimeter - command)) == 0) {
    *match = true;
    reset_state(app_state);
    app_state->state = 'l';
    sprintf(command_message, "Entering logo mode\n");
  }
}

void com_prev(state *app_state, char *command, char *delimeter, bool *match,
              char *error, char * info, char * command_message) {
  if (strncmp(COMMAND_PREV, command, (delimeter - command)) == 0) {
    *match = true;
    if (app_state->state != 'h') {
      sprintf(error, "enter headline mode before changing article\n");
      return;
    }
    if (app_state->selected_article > 0) {
      app_state->selected_article--;
      sprintf(command_message, "loading previous article\n");
    } else {
      sprintf(error, "no previous articles\n");
    }
  }
}

void com_scroll(state *app_state, char *command, char *delimeter, bool *match,
                char *error, char * info, char * command_message) {
  if (strncmp(COMMAND_SCROLL, command, (delimeter - command)) == 0) {
    *match = true;

    if (!delimeter) {
      sprintf(error, "WRONG COMMAND FORMAT");
      return;
    }

    if (app_state->state == 'r') {
      if (strncmp("up", delimeter + 1, 2) == 0) {
        app_state->current_reading_start =
            app_state->articles_storage[app_state->selected_article]->body;
      } else if (strncmp("down", delimeter + 1, 4) == 0) {
        if (app_state->current_reading_end <
            app_state->articles_storage[app_state->selected_article]->body +
                app_state->articles_storage[app_state->selected_article]->size) {
          app_state->current_reading_start = app_state->current_reading_end;
        } else {
          sprintf(error, "You are at the end of the article\n");
        }
      } else {
        sprintf(error, "Unknown parameter\n");
      }

    } else {
      sprintf(error, "SCROLL works in read mode only\n");
    }
  }
}

void com_next(state *app_state, char *command, char *delimeter, bool *match,
              char *error, char * info, char * command_message) {
  if (strncmp(COMMAND_NEXT, command, (delimeter - command)) == 0) {
    *match = true;
    if (app_state->state != 'h') {
      sprintf(error, "enter headline mode before changing article\n");
      return;
    }
    if (app_state->selected_article < app_state->articles_count - 1) {
      app_state->selected_article++;
      sprintf(command_message, "loading next article\n");
    } else {
      sprintf(error, "you reached the end of your article list\n");
    }
  }
}

void com_help(state *app_state, char *command, char *delimeter, bool *match,
              char *error, char * info, char * command_message) {
  if (strncmp(COMMAND_HELP, command, (delimeter - command)) == 0) {
    *match = true;
    sprintf(command_message, "HELP\n");
    sprintf(info,"%s", app_state->usage_file);
  }
}

/**
 * @brief parse command 
 * 
 * @param app_state 
 * @param command 
 */
void parse_command(state *app_state, char *command) {
  app_state->unknown_command = false;


  char *delimeter = strrchr(command, COMMAND_DELIMETER);
  printf("%s COMMAND PARSER\ncommand: %s\n",DIVIDER_SMALL, command);
  char error[255]; error[0] = 0;
  char info[2500]; info[0] = 0;
  char message[255]; message[0] = 0;
  
  
  bool match = false;
  void (*commands[])(state * app_state, char *command, char *delimeter,
                     bool *match, char *error, char * info, char * command_message) = {
      com_scale, com_color, com_read,   com_logo,
      com_back,  com_prev,  com_scroll, com_next, com_help, com_quit};

  for (int i = 0; i < sizeof(commands) / sizeof(command) && !match; i++) {
    commands[i](app_state, command, delimeter, &match, error, info, message);
  }

  if (!match) {
    printf("%s", START_RED);
    printf("UNKNOWN COMMAND\n");
    printf("%s", END_COLOR);
    
    printf("%s\n", app_state->usage_file);
    
    app_state->unknown_command = true;
  } else if (*error) {
    print_red(error);
  } else {
      printf("%s\n",info);
      print_green(message);
      
  }
  
}

void await_command(state *app_state) {
  char buffer[256];
  printf("WAITING FOR COMMAND >");
  scanf("%s", buffer);
  clear_terminal();
  parse_command(app_state, buffer);
}
