/*******************************************************************
  E-reader for news articles. 


  news_reader.c

  Author:       Ondřej Tůma 
  Copyright (C) 2020  Ondřej Tůma

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

 *******************************************************************/
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "general_tools.h"
#include "display_controller.h"
#include "reader_state.h"
#include "commands.h"

#define DEBUG 1
#define DEBUG_SCROLL_LINE 1
#define DEBUG_RENDERER_READ 1

/**
 * @brief Add article to current app state storage
 * 
 * @param app_state 
 */
void add_article(state *app_state, char *head, char *article_file) {
  article * new = (article*) malloc(sizeof(article));
  new->head = (char *) malloc(1000);
  
  strcpy(new->head, head);
  uint32_t size;
  new->body = load_file(article_file, &size);
  new->size = size;
  app_state->articles_storage[app_state->articles_count++] = new;
}


/**
 * @brief Render part of article that fits on the screen
 * 
 * @param app_state 
 */
void print_cur_article_view(state *app_state) {
  set_font_scale(app_state->font_scale);
  
  if (app_state->current_reading_start == NULL) {
    app_state->current_reading_start = app_state->articles_storage[app_state->selected_article]->body;
  }

  reset_buffer();
  app_state->current_reading_end = fill_display_with_text(app_state->current_reading_start);


  #if DEBUG == 1 && DEBUG_RENDERER_READ == 1

    puts("\033[1;1mSHOWED:       -------------------------------------------\033[0m\033[1;2m");
    write(1, app_state->articles_storage[app_state->selected_article]->body,
        app_state->current_reading_start - app_state->articles_storage[app_state->selected_article]->body);
    putc('\n', stdout);
    puts("\033[1;0m\033[1;1mSHOWING:      -------------------------------------------\033[1;0m");
    write(1, app_state->current_reading_start,
          app_state->current_reading_end - app_state->current_reading_start);
    putc('\n', stdout);
    puts("\033[1;1mNOT SHOWING:  -------------------------------------------\033[0m\033[1;2m");
    printf("%s\n", app_state->current_reading_end);
    printf("\033[1;0m");
  #endif
    print_buffer();
  }


/**
 * @brief set led strip based on scroll in currentlz open article
 * 
 * @param app_state 
 */
void set_led_strip_based_on_article_scroll(state *app_state) {
  int size = app_state->articles_storage[app_state->selected_article]->size;
  int from_start =
      app_state->current_reading_start -
      app_state->articles_storage[app_state->selected_article]->body;
  int currently_showing =
      app_state->current_reading_end - app_state->current_reading_start;
  int to_end = size - from_start - currently_showing;
  int per_pixel = size / 32;

  uint32_t line = 0;
  for (int i = 0; i < 32; i++) {
    if (i >= from_start / per_pixel &&
        i <= (from_start + currently_showing) / per_pixel) {
      line += (1 << (31 - i));
    }
  }

  app_state->led_strip = line;

#if DEBUG && DEBUG_SCROLL_LINE == 1
  puts("SCROLL INFO: -------------------------------------------\n");
  printf(
      "size:               %d\n"
      "from_start:         %d\n"
      "currently_showing   %d\n"
      "to_end:             %d\n"
      "line:               ",
      size, from_start, currently_showing, to_end);
  print_bin(&line, 4);
  putc('\n', stdout);
#endif
  
}
/**
 * @brief render custom logo
 * 
 */
void render_logo() {
  reset_buffer();
  add_rectangle(0, 0, SCREEN_WIDTH - 1, SCREEN_HEIGHT - 1);
  set_font_scale(10);
  add_string(30, 80, "TUMAO");
  print_buffer();
}

/**
 * @brief render display based on app_state
 * 
 * @param app_state 
 */
void render(state *app_state) {
  struct timespec tim, tim2;
  tim.tv_sec = 0;
  tim.tv_nsec = 1000000000;
  nanosleep(&tim, &tim2);
  printf("%s RENDERER\n",DIVIDER_SMALL);
  if (app_state->state == 'h') {
    char *headline =
        app_state->articles_storage[app_state->selected_article]->head;
    render_fit(headline);
    
  } else if (app_state->state == 'r') {
    print_cur_article_view(app_state);
    set_led_strip_based_on_article_scroll(app_state);
    

  } else if (app_state->state == 'l') {
    render_logo();
  } else {
  }

  set_strip(app_state->led_strip);
}

/**
 * @brief add article to app_state article storage
 * 
 * @param app_state 
 */
void add_articles(state *app_state) {
  DIR *dp;
  struct dirent *ep;
  dp = opendir("./articles");
  if (dp != NULL) {
    while ((ep = readdir(dp))) {
      if (strcmp("..", ep->d_name) != 0 && strcmp(".", ep->d_name)) {
        char *filename = calloc(1000, 1);
        snprintf(filename, 1000, "%s%s", "./articles/", ep->d_name);
        add_article(app_state, ep->d_name, filename);
        free(filename);
      }
    }
    (void)closedir(dp);
  } else
    perror("Couldn't open the directory");
}


int main(int argc, char *argv[]) {
  state *app_state = init_state();
    clear_terminal();
  add_articles(app_state);
  init_display();
  render(app_state);
  char * output = (char *) malloc(5000);
  while (1) {
    state_to_string(app_state, output);
    printf("%s PRINTING CURRENT STATE\n%s",DIVIDER_SMALL, output);
    printf("%s\n",DIVIDER_SMALL);
    await_command(app_state);
    if (!app_state->unknown_command) {
      render(app_state);
    }
  }
  
  free(app_state->articles_storage);
  free(app_state);
  free(output);
  return 0;
}