/*******************************************************************
  Controller for MZAPO board periphrals.


  display_controller.h

  Author:       Ondřej Tůma 
  Copyright (C) 2020  Ondřej Tůma

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

 *******************************************************************/

#ifndef DISPLAY_CONTROLLER
#define DISPLAY_CONTROLLER

#define SCREEN_WIDTH 480
#define SCREEN_HEIGHT 320
#define SCREEN_SIZE (SCREEN_WIDTH * SCREEN_HEIGHT)
#define BUFFER_SIZE (SCREEN_SIZE * 2)

// DISPLAY 5b red, 6b green, 5b blue
#define DISPLAY_COLOR_BLACK 0b0000000000000000
#define DISPLAY_COLOR_WHITE 0b1111101111111111
#define DISPLAY_COLOR_RED 0b0111100000000000
#define DISPLAY_COLOR_GREEN 0b0000000111100000
#define DISPLAY_COLOR_BLUE 0b0000000000001111

// DISPLAY 8b red, 8b green, 8b blue
#define COLOR_BLACK 0b0000000000000000
#define COLOR_WHITE 0b1111111111111111
#define COLOR_RED 0b111111110000000000000000
#define COLOR_GREEN 0b000000001111111100000000
#define COLOR_BLUE 0b000000000000000011111111

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "font_types.h"

/**
 * @brief rgb to 16 bit display color
 *
 * @param r red rgb value [0-255]
 * @param g gree rgb value [0-255]
 * @param b blue rgb value [0-255]
 * @return uint16_t output 16 bit color
 */
uint16_t rgb_to_pixel(unsigned char, unsigned char, unsigned char);

/**
 * @brief print current buffer to screen
 *
 */
void print_buffer();

/**
 * @brief fill buffer with diven color
 *
 * @param color
 */
void fill_buffer(uint16_t);

/**
 * @brief put display to initial state
 *
 */
void init_display();

/**
 * @brief fill buffer with black
 *
 */
void reset_buffer();

/**
 * @brief render straight line
 *
 * @param x0 from x
 * @param y0 from y
 * @param x1 to x
 * @param y1 to y
 */
void add_line(int, int, int, int);

/**
 * @brief render rectangle at given location
 *
 * @param x0 from x coord
 * @param y0 from y coord
 * @param x1 to x coord
 * @param y1 to y coord
 */
void add_rectangle(int, int, int, int);

/**
 * @brief print single letter without checks
 *
 * @param x coordinate x
 * @param y coordinate y
 * @param c character to print
 */
void add_letter(int, int, char);

/**
 * @brief Print string at selected location without checks
 *
 * @param x
 * @param y
 * @param string
 */
void add_string(int, int, char *);

/**
 * @brief Set the font size
 *
 * @param font
 */
void set_font(font_descriptor_t);

/**
 * @brief Set the led strip
 *
 * @param value
 */
void set_strip(uint32_t);

/**
 * @brief Set the left led
 *
 * @param color
 */
void set_left_led(uint32_t);

/**
 * @brief Set the right led
 *
 * @param color
 */
void set_right_led(uint32_t);

/**
 * @brief Fill display with text at current scale settings, util it fits
 *
 * @param text
 * @return char* first character that did not fit on the screen
 */
char *fill_display_with_text(char *);

/**
 * @brief Set the display color
 *
 * @param color
 */
void set_display_color(uint16_t);

/**
 * @brief Set the font scale
 *
 * @param scale
 */
void set_font_scale(int);

/**
 * @brief print text with maximum possible scale to fit on the screen
 *
 * @param text
 */
void render_fit(char *);

#endif